

all: install

install:
	apt-get install pbuilder devscripts debhelper cowbuilder git dput fakeroot sudo
	if ! getent passwd eobuilder > /dev/null 2>&1; then useradd eobuilder --home-dir /var/lib/eobuilder --create-home; fi
	python setup.py install
	su eobuilder -p  -c "cp pbuilderrc /var/lib/eobuilder/.pbuilderrc"
	su eobuilder -p  -c "cp dput.cf /var/lib/eobuilder/.dput.cf"
	su eobuilder -p  -c "cp gitconfig /var/lib/eobuilder/.gitconfig"
	cp eobuilder.sh /usr/local/bin/eobuilder
	cp eobuilder.cron /etc/cron.daily/eobuilder

uninstall:
	deluser --remove-home eobuilder || true
	rm /usr/local/bin/eobuilder
	rm /etc/cron.daily/eobuilder

chroots:
	for DIST in bullseye bookworm; do \
		for ARCH in amd64; do \
			mkdir -p /var/cache/pbuilder/$$DIST-$$ARCH/base.cow; \
			DIST=$$DIST ARCH=$$ARCH cowbuilder --create --configfile ./pbuilderrc --keyring ./entrouvert.gpg; \
		done; \
	done

chroots-update:
	for DIST in bullseye bookworm; do \
		for ARCH in amd64; do \
			mkdir -p /var/cache/pbuilder/$$DIST-$$ARCH/base.cow; \
			DIST=$$DIST ARCH=$$ARCH cowbuilder --update --configfile ./pbuilderrc --keyring ./entrouvert.gpg; \
		done; \
	done
