#!/bin/sh

if [ "$(whoami)" != "eobuilder" ]; then
    if which sudo; then
      if sudo -v -u eobuilder; then
        sudo -H -u eobuilder eobuilder "$@"
        exit $?
      fi
      echo "You must run this script with eobuilder user"
      exit 1
    fi
fi

EOBUILDER_SETTINGS_FILE=/etc/eobuilder-settings.py eobuilder-ctl $*
