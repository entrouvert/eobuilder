import os
import shutil
import subprocess
import sys

from optparse import OptionParser
from optparse import Option


class MultipleOption(Option):

    ACTIONS = Option.ACTIONS + ('extend',)
    STORE_ACTIONS = Option.STORE_ACTIONS + ('extend',)
    TYPED_ACTIONS = Option.TYPED_ACTIONS + ('extend',)
    ALWAYS_TYPED_ACTIONS = Option.ALWAYS_TYPED_ACTIONS + ('extend',)

    def take_action(self, action, dest, opt, value, values, parser):
        if action == 'extend':
            values_list = values.ensure_value(dest, [])
            for lvalue in value.split(','):
                if not lvalue in values_list:
                    values_list.append(lvalue)
        else:
            Option.take_action(self, action, dest, opt, value, values, parser)


def parse_cmdline():
    parser = OptionParser(
        option_class=MultipleOption,
        usage='usage: %prog [OPTIONS] -d [bullseye|,bookworm] GIT_REPOSITORY_NAME',
    )
    parser.add_option(
        '-a',
        '--architectures',
        action='extend',
        type='string',
        dest='architectures',
        metavar='ARCHITECTURES',
        default=[],
        help='ARCHITECTURES: amd64',
    )
    parser.add_option(
        '-d',
        '--distribution',
        action='extend',
        type='string',
        dest='distrib',
        metavar='DISTRIBUTIONS',
        default=[],
        help='DISTRIBUTIONS: bullseye and/or bookworm',
    )
    parser.add_option(
        '-f', '--force', action='store_true', dest='force', default=False, help='force a new build'
    )
    parser.add_option(
        '-n', '--native', action='store_true', dest='native', default=False, help='build native package'
    )
    parser.add_option(
        '-b',
        '--branch',
        action='store',
        type='string',
        dest='branch',
        metavar='NAME',
        help='branch to build (Default: main, or master)',
    )
    parser.add_option(
        '--epoch',
        action='store',
        type='string',
        dest='epoch',
        metavar='EPOCH',
        default='',
        help='version number epoch (default: none)',
    )
    parser.add_option(
        '-D',
        '--debian-folder',
        action='store',
        type='string',
        dest='debian_folder',
        metavar='NAME',
        default='debian',
        help='debian folder to use for build (Default: debian-DIST or debian)',
    )
    parser.add_option(
        '-c',
        '--clean',
        action='extend',
        type='string',
        dest='cleaning',
        metavar='CLEANING_METHODS',
        default=[],
        help='CLEANING_METHODS: git, deb, archives, smart  and / or all',
    )
    parser.add_option(
        '--hotfix', action='store_true', dest='hotfix', default=False, help='upload to hotfix repository'
    )
    parser.add_option(
        '-r',
        '--repository',
        action='extend',
        type='string',
        default=[],
        dest='repositories',
        metavar='DISTRIBUTION:REPOSITORY, DISTRIBUTION:REPOSITORY',
        help='DISTRIBUTION:REPOSITORY: bullseye:bullseye-eobuilder, bookworm:bookworm-eobuilder',
    )
    parser.add_option(
        '--no-dput',
        dest='dput',
        action='store_false',
        default=True,
        help='do not send package to repository with dput',
    )

    (options, args) = parser.parse_args()

    if len(args) != 1 and not options.cleaning:
        parser.error('you should select one GIT_REPOSITORY_NAME')

    if len(args) and options.cleaning:
        parser.error("you shouldn't use argument when cleaning")

    if len(args) and not options.distrib:
        options.distrib = ['bullseye']

    if options.repositories:
        for r in options.repositories:
            if not ':' in r:
                parser.error('you must enter DISTRIBUTION:REPOSITORY in repository option')

    if not options.architectures:
        options.architectures = ['amd64']
    return options, args


def cat(file_path):
    with open(file_path, 'r') as f:
        content = f.read()
    return content


def touch(fname):
    print('TOUCH:', fname)
    with open(fname, 'a'):
        os.utime(fname, None)


def error(msg, build_dir=None, exit_code=1):
    if build_dir and os.path.exists(build_dir):
        shutil.rmtree(build_dir)
    sys.stderr.write('ERROR: %s\n' % msg)
    sys.exit(exit_code)


def call(cmd):
    """cmd: command line"""
    print('CALL:', cmd)
    rcode = subprocess.call(cmd, shell=True)
    if rcode != 0:
        error(cmd)


def output(cmd, print_output=False, exit_on_error=True):
    """cmd: command line
    print_output: print stdout and stderr
    return outputs (stderr + stdout) as byte string
    """
    output = None
    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        if e.output:
            sys.stderr.write(e.output.decode('utf-8'))
        if exit_on_error:
            error(cmd)
    if print_output:
        print(output)
    return output.decode('utf-8') if output else None


def setup_py(args):
    python = 'python'
    setup_py_contents = open('setup.py').read()
    if 'python3' in setup_py_contents or 'Programming Language :: Python :: 3' in setup_py_contents:
        python = 'python3'
    elif os.path.exists('debian/control') and 'python3-' in open('debian/control').read():
        python = 'python3'
    return output(python + ' setup.py ' + args)
