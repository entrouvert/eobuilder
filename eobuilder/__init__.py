import os

VERSION = '7'


def init():
    print('+ Init EO Builder')
    if not os.path.exists(settings.GIT_PATH):
        os.makedirs(settings.GIT_PATH, 0o755)
    if not os.path.exists(settings.ORIGIN_PATH):
        os.makedirs(settings.ORIGIN_PATH, 0o755)
    if not os.path.exists(settings.PBUILDER_RESULT):
        os.makedirs(settings.PBUILDER_RESULT, 0o755)
    if not os.path.exists(settings.LOCK_PATH):
        os.makedirs(settings.LOCK_PATH, 0o755)
