import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

GIT_PATH = '/var/lib/eobuilder/git'
ORIGIN_PATH = '/var/lib/eobuilder/origin'
LOCK_PATH = '/var/lib/eobuilder/lock'
PBUILDER_RESULT = '/var/lib/eobuilder/results'
EOBUILDER_TMP = '/var/tmp/eobuilder'
GIT_REPOSITORY_URL = 'git+ssh://git@repos.entrouvert.org'
DEBIAN_VERSIONS = {
    'bookworm': '120',
    'bullseye': '110',
    'buster': '100',
    'stretch': '90',
}

DEFAULT_UNSTABLE_REPOSITORIES = {
    'bookworm': 'bookworm-eobuilder',
    'bullseye': 'bullseye-eobuilder',
    'buster': 'buster-eobuilder',
    'stretch': 'stretch-eobuilder',
}

# Paste new tag into repositories
# Automatically add new tag in this repositories
DEFAULT_TESTING_REPOSITORIES = {
    'bookworm': ['bookworm-testing'],
    'bullseye': ['bullseye-testing'],
    'buster': ['buster-testing'],
    'stretch': ['stretch-testing'],
}

# Hotfix repositories
HOTFIX_REPOSITORIES = {
    'bookworm': 'bookworm-hotfix',
    'bullseye': 'bullseye-hotfix',
    'buster': 'buster-hotfix',
    'stretch': 'stretch-hotfix',
}

# Specify the testing repository by source package name
MANUAL_TESTING_REPOSITORIES = {}

# Smart Cleaning
# Keep at least X package versions by project
MIN_PACKAGE_VERSIONS = 10
# Keep a package version during minum X days
MIN_AGE = 60


local_settings_file = os.environ.get(
    'EOBUILDER_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as f:
        exec(f.read())
