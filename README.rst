=======
README
=======

Requirements
------------

You must use a Debian OS. You need to install make.

Installation
------------

Install eobuilder::

    sudo make install

Install chroots (if you want i386 - amd64 wheezy and squeeze)::

    sudo make chroots

Configuration
-------------

Generate a ssh key for eobuilder user or use an existing one.
Configure your Debian repository to allow this key.

You need to add this line into your sudoers::

    eobuilder       ALL=  SETENV:NOPASSWD:/usr/sbin/cowbuilder

Quick start
------------

Example with python-entrouvert::

    su eobuilder -p -c "eobuilder -a amd64 -d squeeze,wheezy wcs"
    su eobuilder -p -c "eobuilder -a amd64 -d wheezy auquotidien"
    su eobuilder -p -c "eobuilder -a i386,amd64 -d squeeze lasso"
    su eobuilder -p -c "eobuilder -c deb"

Note that the SSH host key verification will fail unless the proper
known_hosts file has been installed, or created by manually connecting
to the git server.

Code Style
----------

black is used to format the code, using thoses parameters:

    black --target-version py37 --skip-string-normalization --line-length 110

There is .pre-commit-config.yaml to use pre-commit to automatically run black
before commits. (execute `pre-commit install` to install the git hook.)

Packaging
---------

To make your git repository compatible with eobuilder you need to add a Debian branch
with a debian folder.
You can also use debian-DIST branches.

EO Builder supports 3 types of sources at the moment :

Python
    You need at least a setup.py which supports options :
    --name, --version, --fullname, sdist --formats=bztar and clean --all
    Example: http://repos.entrouvert.org/portail-citoyen-announces.git

Autotools
    You need at least an autogen.sh and a configure.ac files. You also need a script to generate
    version numbers like : http://repos.entrouvert.org/lasso.git/tree/tools/git-version-gen
    Example: http://repos.entrouvert.org/lasso.git

Makefile
    You need to have a Makefile file with at least the following targets :
    version, name, fullname and dist-bzip2 targets


Exit Codes
----------

- 0: success
- 1: unspecified error
- 2: unsupported project type
